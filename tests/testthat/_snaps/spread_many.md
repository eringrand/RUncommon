# spread_many is deprecated

    Code
      fruits <- data.frame(case = c(1, 1, 2, 2), fruit = rep(c("Apple", "Orange"), 2), height = c(4, 0, 1, 2), width = c(
        0, 2, 5, 6))
      x <- spread_many(fruits, fruit, width, height)
    Condition
      [1m[33mWarning[39m:[22m
      `spread_many()` was deprecated in RUncommon 1.2.
      Please use `tidyr::pivot_wider()` instead.
    Message
      Note: Using an external vector in selections is ambiguous.
      i Use `all_of(key_col)` instead of `key_col` to silence this message.
      i See <https://tidyselect.r-lib.org/reference/faq-external-vector.html>.
      This message is displayed once per session.
    Code
      data.frame(case = c(1, 2), Apple_height = c(4L, 1L), Apple_width = c(0L, 5L), Orange_height = c(0L, 2L),
      Orange_width = c(2L, 6L))
    Output
        case Apple_height Apple_width Orange_height Orange_width
      1    1            4           0             0            2
      2    2            1           5             2            6

